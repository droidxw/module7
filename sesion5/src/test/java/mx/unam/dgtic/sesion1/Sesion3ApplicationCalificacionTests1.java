package mx.unam.dgtic.sesion1;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;


@SpringBootTest
//crea los archivos en la base de datos, se comenta una vez creada 
//@Sql({"/schema.sql", "/data.sql" })

//Hector de la Torre
class Sesion3ApplicationCalificacionTests1 {
	
	
	private static final String MATERIA="BD";
	private static final int CALIFICACION=8;
	
@Autowired//apunta a constructor de clase
AlumnoRepository repositorioAlumno;

@Autowired
CalificacionRepository repositorioCalificacion;

	@Test
	void buscarTodosCalifTest() {
		Iterable<Calificacion> iterable = repositorioCalificacion.findAll();
		List<Calificacion> milista= new ArrayList<Calificacion>();
		
		System.out.println("+findAll");
		iterable.forEach(milista::add);//se hace la invocacion del método dentro del iterable (sin necesidad de un parametro)
		iterable.forEach(System.out::println);
		assertThat(milista.size(), greaterThan(0));
	}
	
	

	@Test
	void buscarporMateriaTest() {
		
		List<Calificacion> lista= repositorioCalificacion.findByMateria(MATERIA);
		
		System.out.println("+findByMateria");
		lista.forEach(System.out::println);
		
		 lista= repositorioCalificacion.findByCalificacion(CALIFICACION);
		 
			System.out.println("+findByCalificacion");
			lista.forEach(System.out::println);
		
		
		assertThat(lista.size(), greaterThan(0));
	}
	
	
	
	@Test
	void buscarporAlumnoNombreTest() {
		
		List<Calificacion> lista= repositorioCalificacion.findByAlumnoNombre("Nadia");
		
		System.out.println("+findByAlumnoNombre");
		lista.forEach(System.out::println);		
		
		
		 lista= repositorioCalificacion.findByAlumnoPaterno("Calles");
		 
			System.out.println("+findByAlumnoPaterno");
			lista.forEach(System.out::println);
		
		
		assertThat(lista.size(), greaterThan(0));
	}
	
	

	

}
