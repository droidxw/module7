package mx.unam.dgtic.sesion1;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/alumnos")
public class AlumnoRestController {
	@Autowired
	private AlumnoRepository repositorioAlumno;

//	@GetMapping
//	public Iterable<Alumno> findAll() {
//		return repositorioAlumno.findAll();
//	}

	@GetMapping("/nombre/{nombre}")
	public List<Alumno> getByNombre(@PathVariable String nombre) {
		return repositorioAlumno.findByNombreContaining(nombre);
	}

	@GetMapping
	public Iterable<Alumno> findAll(@RequestParam Map<String, String> parametros) {
		Iterable<Alumno> alumnos = null;
		int leidos = 0;
		if (parametros.containsKey("nombre")) {
			alumnos = repositorioAlumno.findByNombreContaining(parametros.get("nombre"));
			leidos = 1;
		}

		if (parametros.containsKey("paterno")) {
			alumnos = repositorioAlumno.findByPaternoContaining(parametros.get("paterno"));
			leidos = 1;
		}

		if (parametros.containsKey("estatura")) {
			alumnos = repositorioAlumno.findByEstaturaGreaterThanEqual(Double.parseDouble(parametros.get("estatura")));
			leidos = 1;
		}

		if (parametros.containsKey("nombre") && parametros.containsKey("paterno")) {
			alumnos = repositorioAlumno.getByNombreAndPaterno(parametros.get("nombre"), parametros.get("paterno"));
			leidos = 1;
		}

		if (leidos == 0) {
			alumnos = repositorioAlumno.findAll();
		}

		return alumnos;
	}

	@GetMapping("/{matricula}")
	public ResponseEntity<Alumno> buscarMatricula(@PathVariable String matricula) {

		Optional<Alumno> optional = repositorioAlumno.findById(matricula);
		if (optional.isPresent()) {

			Alumno alumno = optional.get();

			return new ResponseEntity<Alumno>(alumno, HttpStatus.OK);
		} else {

			return new ResponseEntity<Alumno>(HttpStatus.NOT_FOUND);
		}

	}

	@DeleteMapping("/{matricula}")
	public ResponseEntity<Alumno> eliminarMatricula(@PathVariable String matricula) {

		Optional<Alumno> optional = repositorioAlumno.findById(matricula);
		if (optional.isPresent()) {
			repositorioAlumno.deleteById(matricula);

			return new ResponseEntity<Alumno>(HttpStatus.OK);
		} else {

			return new ResponseEntity<Alumno>(HttpStatus.NOT_FOUND);
		}

	}
	
	@PostMapping()
	@ResponseStatus(HttpStatus.CREATED)
	public void insertarAlumno(@RequestBody Alumno alumno ) {
		
		repositorioAlumno.save(alumno);
	}
	
	@PutMapping("/{matricula}")
	@ResponseStatus(HttpStatus.OK)
	public void actualizarAlumno(@PathVariable String matricula, @RequestBody Alumno alumno  ) {
		Optional<Alumno> optional = repositorioAlumno.findById(matricula);
		if (optional.isPresent()) {
			Alumno a=alumno;
			a.setMatricula(matricula);	
			
		repositorioAlumno.save(a);
	}
	
	}
}
