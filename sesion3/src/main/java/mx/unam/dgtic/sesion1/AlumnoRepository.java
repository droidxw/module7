package mx.unam.dgtic.sesion1;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

//entidad-tipo en generics
public interface AlumnoRepository extends CrudRepository <Alumno, String> {
	
	//Metodos de busqueda
//	findByNombre(String nombre)
//	getByNombre(String nombre)
//	searchByNombre(String nombre)
//	streamByNombre(String nombre)
	public List<Alumno> getByNombre(String nombre); 	
//	public List<Alumno> getByNombreIs(String nombre); 
//	public List<Alumno> getByNombreEquals(String nombre); 
//	public List<Alumno> findByNombreIs(String nombre); 
	
	public List<Alumno> searchByPaterno(String paterno); 
	public List<Alumno> streamByEstatura(double estatura); 
	
	//negacion
	public List<Alumno> findByNombreNot(String nombre); 

	
	//	null	
	public List<Alumno> findByPaternoIsNull(); 
	public List<Alumno> findByPaternoIsNotNull(); 
	
//	similitud
	public List<Alumno> findByPaternoStartingWith(String prefix); 
	public List<Alumno> findByPaternoEndingWith(String sufix); 
	public List<Alumno> findByPaternoContaining(String infix); 
	
//	%terminacualquiercadena r%empieza  %d% contiene
	public List<Alumno> findByPaternoLike(String pattern); 
	public List<Alumno> findByPaternoNotLike(String pattern); 
	
//	comparacion
//	public List<Alumno> findByEstaturaIsLessThan(double estatura); 
	public List<Alumno> findByEstaturaLessThan(double estatura); 
	public List<Alumno> findByEstaturaLessThanEqual(double estatura); 
	public List<Alumno> findByEstaturaGreaterThan(double estatura);
	public List<Alumno> findByEstaturaGreaterThanEqual(double estatura);

//	public List<Alumno> findByEstaturaGreaterThanEqualNot(double estatura); no funciono
	public List<Alumno> findByEstaturaBetween(double estaturaini, double estaturafin);
	public List<Alumno> findByEstaturaIn(Collection <Double> estatura);
	public List<Alumno> findByEstaturaNotIn(Collection <Double> estatura);
	
//	fecha
//	public List<Alumno> findByFnacIsAfter(Date fecha);
	public List<Alumno> findByFnacAfter(Date fecha);
	public List<Alumno> findByFnacBefore(Date fecha);
	
	
	//And /Or
	public List<Alumno> getByNombreAndPaterno(String nombre,String paterno);
	public List<Alumno> findByNombreOrPaterno(String nombre,String paterno);
	
	
	//Orden
	public List<Alumno> findByNombreOrderByPaterno(String nombre);
	public List<Alumno> findByNombreOrderByPaternoDesc(String nombre);
	
	
	//limit
	public List<Alumno> findFirstByOrderByEstatura();
	public List<Alumno> findTopByOrderByEstaturaDesc();
	
//	count
	long countByEstatura (double estatura);
	long countByEstaturaGreaterThan (double estatura);
	long countByNombreEndingWith (String termina);	
	long countByNombreLike (String pattern);
	
//	distinct
	public List<Alumno> findAlumnoDistinctByNombre(String nombre);
	
	//namedQuery
	public List<Alumno> buscarAltos();
	public List<Alumno> buscarAltosConFecha(Date fecha);
	
	public List<Alumno> buscarPorNombre(String nombre);
	public List<Alumno> buscarPorNombreAndPaterno(String nombre, String paterno);
	
	
	//@Query
	@Query("select avg(a.estatura) from Alumno a")
	public double buscarEstaturaPromedioAlumnos();
	
	@Query (value="select * from alumnos order by nombre", nativeQuery=true)
	public List <Alumno> buscarTodosNative();
	
	@Query (value="select * from alumnos where nombre=? and paterno =? order by nombre", nativeQuery=true)
	public List <Alumno> buscarPorNombreAndPaternoNative(String nombre,String paterno);
	
	//NamedNativeQuery
	public List<Alumno> buscarAlturaMayorPromedio();
	
	//Join Fetch
	public List<Alumno> buscarTodoConCalificaciones();
	
}
