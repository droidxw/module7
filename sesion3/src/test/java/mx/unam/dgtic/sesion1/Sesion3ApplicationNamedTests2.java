package mx.unam.dgtic.sesion1;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.hibernate.boot.jaxb.hbm.spi.JaxbHbmLazyWithExtraEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;



@SpringBootTest
//crea los archivos en la base de datos, se comenta una vez creada 
//@Sql({"/schema.sql", "/data.sql" })

//Hector de la Torre
class Sesion3ApplicationNamedTests2 {

	private static final String MATERIA = "BD";
	private static final int CALIFICACION = 8;

	@Autowired // apunta a constructor de clase
	AlumnoRepository repositorioAlumno;

	@Autowired
	CalificacionRepository repositorioCalificacion;

	@Test
	void buscarAltosTest() {
		List<Alumno> lista = repositorioAlumno.buscarAltos();

		System.out.println("+buscarAltos");
		lista.forEach(System.out::println);

		try {
			lista = repositorioAlumno.buscarAltosConFecha(new SimpleDateFormat("yyyy-MM-dd").parse("2001-01-01"));

			System.out.println("+buscarAltosConFecha");
			lista.forEach(System.out::println);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertThat(lista.size(), greaterThan(0));
	}

	@Test
	void buscarporAlumnoNombreTest() {

		double media = repositorioAlumno.buscarEstaturaPromedioAlumnos();

		System.out.println("+buscarEstaturaPromedioAlumnos" + media);

		assertThat(media, greaterThan(1.0));
	}

	@Test
	void buscarTodosNativoTest() {
		List<Alumno> lista = repositorioAlumno.buscarTodosNative();

		System.out.println("+buscarTodosNative");
		lista.forEach(System.out::println);

		lista = repositorioAlumno.buscarPorNombreAndPaternoNative("Perla", "Calles");

		System.out.println("+buscarPorNombreAndPaterno");
		lista.forEach(System.out::println);

		assertThat(lista.size(), greaterThan(0));
	}

	@Test
	void buscarAlturaMayorPromedioTest() {
		List<Alumno> lista = repositorioAlumno.buscarAlturaMayorPromedio();

		System.out.println("+buscarAlturaMayorPromedio");
		lista.forEach(System.out::println);

		assertThat(lista.size(), greaterThan(0));
	}
	
	
	@Test
	void buscarTodoCalificacionesTest() {
		List<Alumno> lista = repositorioAlumno.buscarTodoConCalificaciones();

		System.out.println("+buscarTodoConCalificaciones");
		lista.forEach(System.out::println);
		
		for(Alumno a:lista) {
			System.out.println(a.getNombre());
			List <Calificacion> calificaciones=a.getCalificaciones();
			
//			trae todos los alumnos con materias y calificaciones
//			calificaciones.forEach(System.out::println);
			
			for(Calificacion c:calificaciones) {
				
				System.out.println(c.getMateria()+""+c.getCalificacion());
			}
			
			
		}

		assertThat(lista.size(), greaterThan(0));
	}

}
