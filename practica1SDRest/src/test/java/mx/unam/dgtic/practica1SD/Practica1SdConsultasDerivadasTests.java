package mx.unam.dgtic.practica1SD;


import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;



@SpringBootTest
public class Practica1SdConsultasDerivadasTests {
	private static final String TIPO="desk";
	private static final double ESTATURA=1.65;
	private static final String SUFFIX="Paterno";
	
	@Autowired
	ComputadoraRepository computadoraRepositorio;

		@Test
		void buscarPorNombreTest() {
			Iterable<Computadora> iterable = computadoraRepositorio.getByEquipo("desk");
			List<Computadora> milista= new ArrayList<Computadora>();
			
			System.out.println("+getByNombre");
			iterable.forEach(milista::add);//se hace la invocacion del método dentro del iterable (sin necesidad de un parametro)
			iterable.forEach(System.out::println);
			assertThat(milista, hasItem(new Computadora("1")));


		}
	
	

}
