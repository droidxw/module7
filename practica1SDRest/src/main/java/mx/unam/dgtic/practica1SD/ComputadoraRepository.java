package mx.unam.dgtic.practica1SD;

import java.util.Collection;
import java.util.List;

import org.springframework.data.repository.CrudRepository;



//entidad-tipo en generics
public interface ComputadoraRepository extends CrudRepository <Computadora, String> {

	public List<Computadora> getByEquipo(String equipo);	
	
	public List<Computadora> getByFabricante(String fabricante);	
	
	
	public List<Computadora> getByClave(String clave);

	public List<Computadora> getByExistencias(Integer tipoexistencias);
	
	
	// And /Or
		public List<Computadora> getByEquipoAndFabricante(String equipo, String fabricante);

	
	
}
