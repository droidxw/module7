package mx.unam.dgtic.practica1SD;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
//mapeo clase-tabla entidad que mapea alumnos
@Table(name = "t_tipo_computadora")
public class Computadora {

	@Id
	@Column(name = "id_computadora")
	private String idComputadora;
	@Column(name = "tipo_equipo")
	private String equipo;
	@Column(name = "tipo_fabricante")
	private String fabricante;
	@Column(name = "tipo_clave")
	private String clave;
	@Column(name = "tipo_existencias")
	private Integer existencias;

	public Computadora() {
		super();
	}
	
	

	public Computadora(String idComputadora) {
		super();
		this.idComputadora = idComputadora;
	}



	public Computadora(String idcomputadora, String equipo, String fabricante, String clave,
			Integer existencias) {
		super();
		this.idComputadora = idcomputadora;
		this.equipo = equipo;
		this.fabricante = fabricante;
		this.clave = clave;
		this.existencias = existencias;
	}

	public String getIdComputadora() {
		return idComputadora;
	}

	public void setIdComputadora(String idComputadora) {
		this.idComputadora = idComputadora;
	}

	public String getEquipo() {
		return equipo;
	}

	public void setEquipo(String Equipo) {
		this.equipo = Equipo;
	}

	public String getFabricante() {
		return fabricante;
	}

	public void setFabricante(String Fabricante) {
		this.fabricante = Fabricante;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String Clave) {
		this.clave = Clave;
	}

	public Integer getExistencias() {
		return existencias;
	}

	public void setExistencias(Integer Existencias) {
		this.existencias = Existencias;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idComputadora == null) ? 0 : idComputadora.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Computadora other = (Computadora) obj;
		if (idComputadora == null) {
			if (other.idComputadora != null)
				return false;
		} else if (!idComputadora.equals(other.idComputadora))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Computadora [idComputadora=" + idComputadora + ", Equipo=" + equipo + ", Fabricante="
				+ fabricante + ", Clave=" + clave + ", Existencias=" + existencias + "]";
	}

}
