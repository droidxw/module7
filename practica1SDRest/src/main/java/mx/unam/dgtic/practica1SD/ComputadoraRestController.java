package mx.unam.dgtic.practica1SD;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/computadoras")
public class ComputadoraRestController {
	@Autowired
	private ComputadoraRepository computadoraRepository;

//	@GetMapping
//	public Iterable<Computadora> findAll() {
//		return computadoraRepository.findAll();
//	}

	@GetMapping("/fabricante/{fabricante}")
	public List<Computadora> getByTipoFabricante(@PathVariable String fabricante) {
		return computadoraRepository.getByFabricante(fabricante);
	}

	@GetMapping
	public Iterable<Computadora> findAll(@RequestParam Map<String, String> parametros) {
		Iterable<Computadora> alumnos = null;
		int leidos = 0;
		if (parametros.containsKey("equipo")) {
			alumnos = computadoraRepository.getByEquipo(parametros.get("equipo"));
			leidos = 1;
		}

		if (parametros.containsKey("fabricante")) {
			alumnos = computadoraRepository.getByFabricante(parametros.get("fabricante"));
			leidos = 1;
		}

		if (parametros.containsKey("clave")) {
			alumnos = computadoraRepository.getByClave((parametros.get("clave")));
			leidos = 1;
		}

		if (parametros.containsKey("equipo") && parametros.containsKey("fabricante")) {
			alumnos = computadoraRepository.getByEquipoAndFabricante(parametros.get("equipo"), parametros.get("fabricante"));
			leidos = 1;
		}

		if (leidos == 0) {
			alumnos = computadoraRepository.findAll();
		}

		return alumnos;
	}
//
//	@GetMapping("/{matricula}")
//	public ResponseEntity<Computadora> buscarMatricula(@PathVariable String matricula) {
//
//		Optional<Computadora> optional = computadoraRepository.findById(matricula);
//		if (optional.isPresent()) {
//
//			Computadora alumno = optional.get();
//
//			return new ResponseEntity<Computadora>(alumno, HttpStatus.OK);
//		} else {
//
//			return new ResponseEntity<Computadora>(HttpStatus.NOT_FOUND);
//		}
//
//	}
//

	
	@PostMapping()
	@ResponseStatus(HttpStatus.CREATED)
	public void insertarComputadora(@RequestBody Computadora equipo ) {
		
		computadoraRepository.save(equipo);
	}
	
	@PutMapping("/{clave}")
	@ResponseStatus(HttpStatus.OK)
	public void actualizarEquipo (@PathVariable String clave, @RequestBody Computadora equipo  ) {
		Optional<Computadora> optional = computadoraRepository.findById(clave);
		if (optional.isPresent()) {
			Computadora a=equipo;
			a.setClave(clave);	
			
		computadoraRepository.save(a);
	}
	
	}
	
	
	@DeleteMapping("/{clave}")
	public ResponseEntity<Computadora> eliminarMatricula(@PathVariable String clave) {

		Optional<Computadora> optional = computadoraRepository.findById(clave);
		if (optional.isPresent()) {
			computadoraRepository.deleteById(clave);

			return new ResponseEntity<Computadora>(HttpStatus.OK);
		} else {

			return new ResponseEntity<Computadora>(HttpStatus.NOT_FOUND);
		}

	}
}
