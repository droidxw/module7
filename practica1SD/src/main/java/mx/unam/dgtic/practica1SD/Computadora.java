package mx.unam.dgtic.practica1SD;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
//mapeo clase-tabla entidad que mapea alumnos
@Table(name = "t_tipo_computadora")
public class Computadora {

	@Id
	@Column(name = "id_computadora")
	private String idComputadora;
	@Column(name = "tipo_equipo")
	private String tipoEquipo;
	@Column(name = "tipo_fabricante")
	private String tipoFabricante;
	@Column(name = "tipo_clave")
	private String tipoClave;
	@Column(name = "tipo_existencias")
	private Integer tipoExistencias;

	public Computadora() {
		super();
	}
	
	

	public Computadora(String idComputadora) {
		super();
		this.idComputadora = idComputadora;
	}



	public Computadora(String idcomputadora, String tipoequipo, String tipofabricante, String tipoclave,
			Integer tipoexistencias) {
		super();
		this.idComputadora = idcomputadora;
		this.tipoEquipo = tipoequipo;
		this.tipoFabricante = tipofabricante;
		this.tipoClave = tipoclave;
		this.tipoExistencias = tipoexistencias;
	}

	public String getIdComputadora() {
		return idComputadora;
	}

	public void setIdComputadora(String idComputadora) {
		this.idComputadora = idComputadora;
	}

	public String getTipoEquipo() {
		return tipoEquipo;
	}

	public void setTipoEquipo(String tipoEquipo) {
		this.tipoEquipo = tipoEquipo;
	}

	public String getTipoFabricante() {
		return tipoFabricante;
	}

	public void setTipoFabricante(String tipoFabricante) {
		this.tipoFabricante = tipoFabricante;
	}

	public String getTipoClave() {
		return tipoClave;
	}

	public void setTipoClave(String tipoClave) {
		this.tipoClave = tipoClave;
	}

	public Integer getTipoExistencias() {
		return tipoExistencias;
	}

	public void setTipoExistencias(Integer tipoExistencias) {
		this.tipoExistencias = tipoExistencias;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idComputadora == null) ? 0 : idComputadora.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Computadora other = (Computadora) obj;
		if (idComputadora == null) {
			if (other.idComputadora != null)
				return false;
		} else if (!idComputadora.equals(other.idComputadora))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Computadora [idComputadora=" + idComputadora + ", tipoEquipo=" + tipoEquipo + ", tipoFabricante="
				+ tipoFabricante + ", tipoClave=" + tipoClave + ", tipoExistencias=" + tipoExistencias + "]";
	}

}
