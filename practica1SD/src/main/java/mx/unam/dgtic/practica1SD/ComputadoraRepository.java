package mx.unam.dgtic.practica1SD;

import java.util.Collection;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

//entidad-tipo en generics
public interface ComputadoraRepository extends CrudRepository <Computadora, String> {

	public List<Computadora> getByTipoEquipo(String tipoequipo);	
	
	public List<Computadora> getByTipoFabricante(String tipofabricante);	
	
	
	public List<Computadora> getByTipoClave(String tipoclave);

	public List<Computadora> getByTipoExistencias(Integer tipoexistencias);

	
	
}
