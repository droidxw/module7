package mx.unam.dgtic.practica1SD;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import mx.unam.dgtic.practica1SD.Computadora;
import mx.unam.dgtic.practica1SD.ComputadoraRepository;

@SpringBootTest
class Practica1SdApplicationTests {

//	@Test
//	void contextLoads() {
//	}

	
	@Autowired//apunta a constructor de clase
	ComputadoraRepository computadoraRepositorio;

		@Test
		void buscarTodosTest() {
			Iterable<Computadora> iterable = computadoraRepositorio.findAll();
			List<Computadora> milista= new ArrayList<Computadora>();
			
			System.out.println("++findAll");
			iterable.forEach(milista::add);//se hace la invocacion del método dentro del iterable (sin necesidad de un parametro)
			iterable.forEach(System.out::println);
			assertThat(milista.size(), greaterThan(0));
		}
		
}
