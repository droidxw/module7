package mx.unam.dgtic.sesion1;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.hibernate.boot.jaxb.hbm.spi.JaxbHbmLazyWithExtraEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.TypedSort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.test.context.jdbc.Sql;



@SpringBootTest
//crea los archivos en la base de datos, se comenta una vez creada 
//@Sql({"/schema.sql", "/data.sql" })

//Hector de la Torre
class Sesion4ApplicationDTOPage1 {

	private static final String MATERIA = "BD";
	private static final int CALIFICACION = 8;

	@Autowired // apunta a constructor de clase
	AlumnoRepository repositorioAlumno;

	@Autowired
	CalificacionRepository repositorioCalificacion;

	@Test
	void buscarTodosDTOTest() {
		List<AlumnoCalificacionDTO> lista = repositorioAlumno.findAlumnoCalificacionDTO();

		System.out.println("+findAlumnoCalificacionDTO()");
		lista.forEach(System.out::println);
		assertThat(lista.size(), greaterThan(0));
	}
	
	
	@Test
	void buscarTodosOrdenadosTest() {
		List<Alumno> lista = repositorioAlumno.findByOrderByNombreAscPaternoDesc();

		System.out.println("+findByOrderByNombreAscPaternoDesc");
		lista.forEach(System.out::println);
		
		lista = repositorioAlumno.findByOrderByNombre();
		
		System.out.println("+findByOrderByNombre");
		lista.forEach(System.out::println);
		assertNotNull(lista);
		
		Iterable<Alumno>it=repositorioAlumno.findAll();
		List<Alumno> lista2 =new ArrayList<Alumno>();
		it.forEach(lista2::add);
		
		List<Alumno> listaOrdenadaAlumnos =lista2.stream().sorted(Comparator.comparing(Alumno::getNombre))
				.collect(Collectors.toList());
		
		assertEquals(lista, listaOrdenadaAlumnos);
//		assertThat(lista.size(), greaterThan(0));
	}

	
	@Test
	void buscarTodosOrdenadosParametrosTest() {
		Iterable<Alumno> it = repositorioAlumno.findAll(Sort.by("nombre"));
		List<Alumno> lista=new ArrayList<Alumno>();
		it.forEach(lista::add);
		System.out.println("+Sort.by(nombre)");
		lista.forEach(System.out::println);
		
		it=repositorioAlumno.findAll(Sort.by("estatura").descending());
		lista=new ArrayList<Alumno>();
		it.forEach(lista::add);
		System.out.println("+Sort.by(estatura)");
		lista.forEach(System.out::println);
		
		it= repositorioAlumno.findAll(Sort.by("nombre").ascending().and(Sort.by("paterno").descending()));
		 lista=new ArrayList<Alumno>();
		it.forEach(lista::add);
		System.out.println("+Sort.by.estatura.descending()");
		lista.forEach(System.out::println);
		
		
		assertThat(lista.size(), greaterThan(0));
	}
	
	
	

	@Test
	void buscarTodosOrdenadosTypedTest() {
		
		TypedSort<Alumno> ordenar= Sort.sort(Alumno.class);
		Sort ordenfinal=ordenar.by(Alumno::getNombre).ascending()
				.and(ordenar.by(Alumno::getPaterno)).ascending();
		
		Iterable<Alumno> it = repositorioAlumno.findAll(ordenfinal);
		List<Alumno> lista = new ArrayList<Alumno>();	
		it.forEach(lista::add);
		System.out.println("+TypedSort nombre");
		lista.forEach(System.out::println);
		assertThat(lista.size(), greaterThan(0));
	}
	
	
	@Test
	void buscarTodosPaginadoTest() {
		Page<Alumno> pagina=repositorioAlumno.findAll(PageRequest.of(0,2,Sort.by("nombre").descending()));
//		Page<Alumno> pagina=repositorioAlumno.findAll(PageRequest.of(0,2));
		List<Alumno> lista =pagina.getContent();

		System.out.println("+Page(0,2)");
		lista.forEach(System.out::println);
		Page<Alumno> siguiente =repositorioAlumno.findAll(pagina.nextPageable());
		lista=siguiente.getContent();
		System.out.println("+Page1");
		lista.forEach(System.out::println);
		assertThat(lista.size(), greaterThan(0));
	}
	

}
