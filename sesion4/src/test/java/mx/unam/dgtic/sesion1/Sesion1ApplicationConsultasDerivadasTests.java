package mx.unam.dgtic.sesion1;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
//Hector de la Torre	
@SpringBootTest
public class Sesion1ApplicationConsultasDerivadasTests {
	private static final String NOMBRE="Maria";
	private static final double ESTATURA=1.65;
	private static final String SUFFIX="Paterno";

	@Autowired
	AlumnoRepository repositorioAlumno;

		@Test
		void buscarPorNombreTest() {
			Iterable<Alumno> iterable = repositorioAlumno.getByNombre(NOMBRE);
			List<Alumno> milista= new ArrayList<Alumno>();
			
			System.out.println("+getByNombre");
			iterable.forEach(milista::add);//se hace la invocacion del método dentro del iterable (sin necesidad de un parametro)
			iterable.forEach(System.out::println);
			assertThat(milista, hasItem(new Alumno("G1")));
//			assertThat(milista.size(), greaterThan(1));

		}
		
		
		
		@Test
		void buscarPorPaternoTest() {
			Iterable<Alumno> iterable = repositorioAlumno.searchByPaterno("Madero");
			List<Alumno> milista= new ArrayList<Alumno>();
			
			System.out.println("+searchByPaterno");
			iterable.forEach(milista::add);//se hace la invocacion del método dentro del iterable (sin necesidad de un parametro)
			iterable.forEach(System.out::println);
//			assertThat(milista, hasItem(new Alumno("G1")));
			assertThat(milista.size(), greaterThan(0));

		}
		
		
		@Test
		void buscarPorEstaturaTest() {
			Iterable<Alumno> iterable = repositorioAlumno.streamByEstatura(ESTATURA);
			List<Alumno> milista= new ArrayList<Alumno>();
			
			System.out.println("+streamByEstatura");
			iterable.forEach(milista::add);//se hace la invocacion del método dentro del iterable (sin necesidad de un parametro)
			iterable.forEach(System.out::println);

			assertThat(milista.size(), greaterThan(0));

		}
		
		
		
		@Test
		void buscarPorNotNombreTest() {
			Iterable<Alumno> iterable = repositorioAlumno.findByNombreNot(NOMBRE);
			List<Alumno> milista= new ArrayList<Alumno>();
			
			System.out.println("+findByNombreNot");
			iterable.forEach(milista::add);//se hace la invocacion del método dentro del iterable (sin necesidad de un parametro)
			iterable.forEach(System.out::println);

			assertThat(milista.size(), greaterThan(0));
			System.out.println("Not Nombre Encontrados: "+ repositorioAlumno.findByNombreNot(NOMBRE).size());

		}
		
		
		@Test
		void buscarPorPaternoNullTest() {
			Iterable<Alumno> iterable = repositorioAlumno.findByPaternoIsNull();
			List<Alumno> milista= new ArrayList<Alumno>();
			
			System.out.println("+findByPaternoIsNull");
			iterable.forEach(milista::add);//se hace la invocacion del método dentro del iterable (sin necesidad de un parametro)
			iterable.forEach(System.out::println);

			assertThat(milista.size(), greaterThan(0));

		}
		
		
		@Test
		void buscarPorPaternoSimilitudTest() {
			Iterable<Alumno> iterable = repositorioAlumno.findByPaternoStartingWith(SUFFIX);
			List<Alumno> milista= new ArrayList<Alumno>();
			
			System.out.println("+findByPaternoStartingWith");
			iterable.forEach(milista::add);//se hace la invocacion del método dentro del iterable (sin necesidad de un parametro)
			iterable.forEach(System.out::println);
			////
		    iterable = repositorioAlumno.findByPaternoEndingWith(SUFFIX);
		    milista= new ArrayList<Alumno>();
			 
		    System.out.println("+findByPaternoEndingWith");
			iterable.forEach(milista::add);//se hace la invocacion del método dentro del iterable (sin necesidad de un parametro)
			iterable.forEach(System.out::println);
			
			////
			iterable = repositorioAlumno.findByPaternoContaining(SUFFIX);
			milista= new ArrayList<Alumno>();
				 
			System.out.println("+findByPaternoContaining");
			iterable.forEach(milista::add);//se hace la invocacion del método dentro del iterable (sin necesidad de un parametro)
			iterable.forEach(System.out::println);
			 
			assertThat(milista.size(), greaterThan(0));

		}

		@Test
		void buscarPorPaternoLikeTest() {
			
			//no lo encuentra "P% %3"
			Iterable<Alumno> iterable = repositorioAlumno.findByPaternoLike("P%3");
			List<Alumno> milista= new ArrayList<Alumno>();
			
			System.out.println("+findByPaternoLike");
			iterable.forEach(milista::add);//se hace la invocacion del método dentro del iterable (sin necesidad de un parametro)
			iterable.forEach(System.out::println);
			
			
			iterable = repositorioAlumno.findByPaternoNotLike("P%3");
			milista= new ArrayList<Alumno>();
				 
			System.out.println("+findByPaternoNotLike");
			iterable.forEach(milista::add);//se hace la invocacion del método dentro del iterable (sin necesidad de un parametro)
			iterable.forEach(System.out::println);

			assertThat(milista.size(), greaterThan(0));

		}
		
		
		
		@Test
		void buscarPorEstaturaMayorTest() {
			Iterable<Alumno> iterable = repositorioAlumno.findByEstaturaGreaterThan(ESTATURA);
			List<Alumno> milista= new ArrayList<Alumno>();
			
			System.out.println("+findByEstaturaGreaterThan");
			iterable.forEach(milista::add);//se hace la invocacion del método dentro del iterable (sin necesidad de un parametro)
			iterable.forEach(System.out::println);
			
//			iterable =repositorioAlumno.findByEstaturaGreaterThan(ESTATURA);
//			 milista= new ArrayList<Alumno>();
//			 
//			 System.out.println("findByEstaturaGreaterThanEqualNot");
//				iterable.forEach(milista::add);//se hace la invocacion del método dentro del iterable (sin necesidad de un parametro)
//				iterable.forEach(System.out::println);

			assertThat(milista.size(), greaterThan(0));

		}
		
		@Test
		void buscarPorEstaturaRangoTest() {
			Iterable<Alumno> iterable = repositorioAlumno.findByEstaturaBetween(ESTATURA, ESTATURA+0.9 );
			List<Alumno> milista= new ArrayList<Alumno>();
			
			System.out.println("+findByEstaturaBetween");
			iterable.forEach(milista::add);//se hace la invocacion del método dentro del iterable (sin necesidad de un parametro)
			iterable.forEach(System.out::println);
			
			/////
			final List<Double> estaturas=Arrays.asList(1.53, ESTATURA, 1.68);
			iterable =repositorioAlumno.findByEstaturaIn(estaturas);
			milista= new ArrayList<Alumno>();
			
			System.out.println("+findByEstaturaIn");
			iterable.forEach(milista::add);//se hace la invocacion del método dentro del iterable (sin necesidad de un parametro)
			iterable.forEach(System.out::println);
			
			/////
			iterable =repositorioAlumno.findByEstaturaNotIn(estaturas);
			milista= new ArrayList<Alumno>();
			
			System.out.println("+findByEstaturaNotIn");
			iterable.forEach(milista::add);//se hace la invocacion del método dentro del iterable (sin necesidad de un parametro)
			iterable.forEach(System.out::println);			
			
			assertThat(milista.size(), greaterThan(0));

		}
		
		
		@Test
		void buscarPorFechaTest() {			
			List<Alumno> milista;
			
				try {
				milista= repositorioAlumno.findByFnacBefore(new SimpleDateFormat("yyyy-MM-dd").parse("2022-01-08"));				
				System.out.println("+findByFnacBefore");			
				milista.forEach(System.out::println);
				///
				milista= repositorioAlumno.findByFnacAfter(new SimpleDateFormat("yyyy-MM-dd").parse("2001-03-20"));				
				System.out.println("+findByFnacAfter");			
				milista.forEach(System.out::println);
	
				assertThat(milista.size(), greaterThan(0));
				}catch(ParseException e) {
					e.printStackTrace();
				}
		}

		
		
		@Test
		void buscarPorNombrePaternoTest() {
//no da nada ya que no hay ninguna maria rios
			List<Alumno> milista= repositorioAlumno.getByNombreAndPaterno(NOMBRE, "Rios");			
			System.out.println("+getByNombreAndPaterno");			
			milista.forEach(System.out::println);
		
			milista= repositorioAlumno.findByNombreOrPaterno(NOMBRE, "Rios");			
			System.out.println("+findByNombreOrPaterno");			
			milista.forEach(System.out::println);
			
		

			assertThat(milista.size(), greaterThan(0));

		}
		
		
		@Test
		void buscarPorNombreOrdenPaternoTest() {
//no da nada ya que no hay ninguna maria rios
			List<Alumno> milista= repositorioAlumno.findByNombreOrderByPaternoDesc(NOMBRE);			
			System.out.println("+findByNombreOrderByPaternoDesc");			
			milista.forEach(System.out::println);
		
			milista= repositorioAlumno.findByNombreOrderByPaterno(NOMBRE);			
			System.out.println("+findByNombreOrderByPaterno");			
			milista.forEach(System.out::println);		

			assertThat(milista.size(), greaterThan(0));

		}
		
		
		
		@Test
		void LimiteTest() {

			List<Alumno> milista= repositorioAlumno.findFirstByOrderByEstatura();			
			System.out.println("+findFirstByOrderByEstatura");			
			milista.forEach(System.out::println);
		
			milista= repositorioAlumno.findTopByOrderByEstaturaDesc();			
			System.out.println("+findTopByOrderByEstaturaDesc");			
			milista.forEach(System.out::println);		

			assertThat(milista.size(), greaterThan(0));

		}
		
		@Test
		void ContarTest() {

			long count= repositorioAlumno.countByEstatura(ESTATURA);			
			System.out.println("+countByEstatura: "+count);			
			
		
			count= repositorioAlumno.countByEstaturaGreaterThan(ESTATURA);			
			System.out.println("+countByEstaturaGreaterThan: "+count);	
			
			count= repositorioAlumno.countByNombreEndingWith("a");			
			System.out.println("+countByNombreEndingWith: "+count);
			
			count= repositorioAlumno.countByNombreLike("%e%");			
			System.out.println("+countByNombreLike: "+count);
				

			assertTrue(count>0);

		}
				
		
		@Test
		//distincion de la entidad no del alumno  trae elementos que coinciden 
		void distinctTest() {

			List<Alumno> milista= repositorioAlumno.findAlumnoDistinctByNombre(NOMBRE);			
			System.out.println("+findAlumnoDistinctByNombre");			
			milista.forEach(System.out::println);			

			assertThat(milista.size(), greaterThan(0));
		}

}
