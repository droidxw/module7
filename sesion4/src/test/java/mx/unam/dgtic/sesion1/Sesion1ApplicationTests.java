package mx.unam.dgtic.sesion1;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;


@SpringBootTest
//crea los archivos en la base de datos, se comenta una vez creada 
//@Sql({"/schema.sql", "/data.sql" })

//Hector de la Torre
class Sesion1ApplicationTests {
@Autowired//apunta a constructor de clase
AlumnoRepository repositorioAlumno;

	@Test
	void buscarTodosTest() {
		Iterable<Alumno> iterable = repositorioAlumno.findAll();
		List<Alumno> milista= new ArrayList<Alumno>();
		
		System.out.println("findAll");
		iterable.forEach(milista::add);//se hace la invocacion del método dentro del iterable (sin necesidad de un parametro)
		iterable.forEach(System.out::println);
		assertThat(milista.size(), greaterThan(6));
	}
	
	
	@Test
	void buscarTodosUnoTest() {
		Optional<Alumno> optional = repositorioAlumno.findById("2A");		
	
		System.out.println("findById");
		if(optional.isPresent()) {
		assertThat(optional.get().getNombre(), equalTo("Nadia"));
		System.out.println(optional.get().toString());
	}
		}
	
	
	@Test
	void insertarUnoTest() {		
		Alumno alumno =new Alumno("1F","Hector","delaTorre", new Date(), 1.74);
		repositorioAlumno.save(alumno);
		Optional<Alumno> optional = repositorioAlumno.findById("1F");		
	
		System.out.println("Save");
		if(optional.isPresent()) {
		assertThat(optional.get().getNombre(), equalTo("Hector"));
		System.out.println(optional.get().toString());
	}
		
		}	
	
	@Test
	void borrarUnoTest() {		
		Alumno alumno =new Alumno("1A");
		repositorioAlumno.delete(alumno);
		Optional<Alumno> optional = repositorioAlumno.findById("1A");		
	
		System.out.println("Delete");		
		assertFalse(optional.isPresent());	
		}
	
	
	
	@Test
	void actualizarUnoTest() {		
		
		Optional<Alumno> optional = repositorioAlumno.findById("3B");	
		if(optional.isPresent()) {
		Alumno alumno =new Alumno();
		alumno=optional.get();
		alumno.setPaterno("Rios");
		repositorioAlumno.save(alumno);
	
		System.out.println("Update");	
		optional=repositorioAlumno.findById("3B");
		System.out.println(optional.get().toString());
		assertThat(optional.get().getPaterno(), equalTo("Rios"));	
	}
		}
	
	
	
	@Test
	void buscarVariosTest() {
		Iterable<Alumno> iterable = repositorioAlumno.findAllById(List.of("2A", "3B"));
		List<Alumno> milista= new ArrayList<Alumno>();
		
		System.out.println("findAllById");
		iterable.forEach(milista::add);
		iterable.forEach(System.out::println);
		assertThat(milista.get(0).getNombre(), equalTo("Nadia"));
	}
	
	
	
	
	@Test
	void insertarVariosTest() {		
		List <Alumno> listaAlumnos=new ArrayList<Alumno>();
		for(int i=1; i<6; i++) {
			listaAlumnos.add(new Alumno("G"+i,"Maria","Paterno"+i,new Date(),1.60+(i/10)));
			
		}
		
		repositorioAlumno.saveAll(listaAlumnos);
		Optional<Alumno> optional = repositorioAlumno.findById("G1");		
	
		System.out.println("SaveAll");
		if(optional.isPresent()) {
		assertThat(optional.get().getNombre(), equalTo("Maria"));
		System.out.println("Numero de alumnos: "+repositorioAlumno.count());
	}
		
		}	
	
}
